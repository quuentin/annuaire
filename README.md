# Symfony Docker

A [Docker](https://www.docker.com/)-based installer and runtime for the [Symfony](https://symfony.com) web framework, with full [HTTP/2](https://symfony.com/doc/current/weblink.html), HTTP/3 and HTTPS support.

![CI](https://github.com/dunglas/symfony-docker/workflows/CI/badge.svg)

## Getting Started

1. If not already done, [install Docker Compose](https://docs.docker.com/compose/install/) (v2.10+)
2. Run `docker compose build --pull --no-cache` to build fresh images
3. Run `docker compose up` (the logs will be displayed in the current shell)
4. Run `docker compose exec php yarn install` (install yarn dependencies)
5. Run `docker compose exec php yarn dev` (build the front)
6. Open `https://localhost` in your favorite web browser and [accept the auto-generated TLS certificate](https://stackoverflow.com/a/15076602/1352334)
7. Run `docker compose down --remove-orphans` to stop the Docker containers.


## License

Symfony Docker is available under the MIT License.

## Ressources

Symfony docker based from https://github.com/dunglas/symfony-docker Created by Kévin Dunglas.

### Documentation

Départ du git en référence ci-dessus, j'ai adapté le DockerFile et le compose en fonction de mes besoins.
Ajout de webpack Encore afin de gérer mes dépendances et les points d'entrées de mon front.

En front end j'utilise VueJS 3 avec Vuetify pour le design, vuelidate pour la validation de formulaires,
sweetalert2 pour la gestion des alertes d'erreurs ou de succès, et axios pour communiquer avec les api de mon backend.

Coté backend j'ai installé apiPlatform qui permet de mettre en place rapidement une API REST, il permet de transformer des entités en ressources API 

Pour la base données j'utilise doctrine comme ORM, et une base Postgres, c'est la base qui vient de base avec DoctrineBundle et ce docker j'aurais pu utiliser d'autre type de db.
